set terminal gif enhanced medium animate delay 4 
set output "anim.gif"
set style line 1 lc rgb '#000000' lt 1 lw 1.5 pt 3 ps 1
set style line 2 lc rgb '#222222' lt 1 lw 1.5 pt 3 ps 1
set style line 3 lc rgb '#444444' lt 1 lw 1.5 pt 3 ps 1
set style line 4 lc rgb '#666666' lt 1 lw 1.5 pt 3 ps 1
set style line 5 lc rgb '#888888' lt 1 lw 1.5 pt 3 ps 1
set style line 6 lc rgb '#AAAAAA' lt 1 lw 1.5 pt 3 ps 1
set style line 7 lc rgb '#CCCCCC' lt 1 lw 1.5 pt 3 ps 1
set style line 8 lc rgb '#EEEEEE' lt 1 lw 1.5 pt 3 ps 1

set xrange [-2:2]
set yrange [-2:2]
sc=100

unset key

do for [i=8:100] {
  plot for[j=1:8] "<paste xy1.txt xy2.txt" every ::(sc*(i-j))::(sc*(i-j)) us 1:2:3:4 with vectors ls j,\
  for[j=1:8] "xy1.txt" every ::(sc*(i-j))::(sc*(i-j)) us (0):(0):1:2 with vectors ls j
}
