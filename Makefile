CC = g++
CFLAGS = -c -g -Wall
GNU = gnuplot



exe: NPendulum
	./Pendulum

graf: exe
	$(GNU) graf.gnuplot


NPendulum: Pendulum.o MainP.o
	$(CC) -o Pendulum $^

Pendulum: Pendulum.C

MainP: MainP.C

.C.o:
	$(CC) $(CFLAGS) $<

clean:
	rm -f *.o
