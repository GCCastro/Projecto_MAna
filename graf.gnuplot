set term pdf
set size 16/9


#Energia vs tempo
set output 'graf_energia.pdf'
plot 'dataEn.txt'

#Ângulo vs tempo
set output 'graf_posicao.pdf'
plot 'data.txt'

#Espaço de fases
set output 'graf_fase.pdf'
plot 'data_fase.txt'
