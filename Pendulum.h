#include <cmath>
#include <cstdlib>
#include <iostream>

#ifndef _PENDULUM_
#define _PENDULUM_

class Pendulum {

  public:
  Pendulum();
  Pendulum(int n1, double l1, double theta0, double vtheta0, double atheta0, int n_it1, double m1);
  ~Pendulum();

  double f_x(int k, int iter, double* vaux, double h);
  double f_w(int k, int iter, double* aux, double* vaux, double h);
  void RungeKutta(double h);

  private:
  double** theta;
  double** vtheta;
  double** atheta;
  double l;
  int n;
  int n_it;
  double m;

};

#endif
